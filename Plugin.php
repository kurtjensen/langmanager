<?php namespace KurtJensen\LangManager;

use Backend;
use System\Classes\PluginBase;

/**
 * LangManager Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'kurtjensen.langmanager::lang.plugin.name',
            'description' => 'kurtjensen.langmanager::lang.plugin.description',
            'author' => 'KurtJensen',
            'icon' => 'icon-leaf',
            'lang' => 'pt-br',
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'KurtJensen\LangManager\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {

        return [
            'kurtjensen.langmanager.*' => [
                'tab' => 'kurtjensen.langmanager::lang.plugin.perm_tab',
                'label' => 'kurtjensen.langmanager::lang.plugin.perm_all',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'langmanager' => [
                'label' => 'kurtjensen.langmanager::lang.plugin.short_name',
                'url' => Backend::url('kurtjensen/langmanager/main'),
                'icon' => 'icon-leaf',
                'permissions' => ['kurtjensen.langmanager.*'],
                'order' => 500,
            ],
            /*,
        'sideMenu' => [
        'langmanager' => [
        'label' => 'LangManager',
        'url' => Backend::url('kurtjensen/langmanager/compare'),
        'icon' => 'icon-leaf',
        'permissions' => ['kurtjensen.langmanager.*'],
        'order' => 500,
        ],
        ],*/
        ];
    }

}

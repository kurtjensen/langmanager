<?php namespace KurtJensen\LangManager\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use System\Classes\PluginManager;

/**
 * Main Back-end Controller
 */
class Main extends Controller
{
    public $implement = [
//        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

//    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('KurtJensen.LangManager', 'langmanager', 'main');
        $this->pageTitle = e(trans('kurtjensen.langmanager::lang.plugin.name'));
    }

    private function getTransKeys($a, $incStrings = false)
    {
        foreach ($a as $k[0] => $v1) {
            if (!is_array($v1)) {
                $code = $k[0];
                $val[$code] = $v1;
            } else {
                foreach ($v1 as $k[1] => $v2) {
                    if (!is_array($v2)) {
                        $code = implode('.', $k);
                        $val[$code] = $v2;
                    } else {
                        foreach ($v2 as $k[2] => $v3) {
                            if (!is_array($v3)) {
                                $code = implode('.', $k);
                                $val[$code] = $v3;
                            } else {
                                foreach ($v3 as $k[3] => $v4) {
                                    if (!is_array($v4)) {
                                        $code = implode('.', $k);
                                        $val[$code] = $v4;
                                    } else {
                                        foreach ($v4 as $k[4] => $v5) {
                                            if (!is_array($v5)) {
                                                $code = implode('.', $k);
                                                $val[$code] = $v5;
                                            }
                                        }
                                    }

                                }

                            }
                        }
                    }
                }
            }
            $k = [];
        }

        return $val;
    }

    private function trimDir(&$dir, $key, $start)
    {
        $dir = substr($dir, $start);
    }

    public function update($urlCode = null)
    {
        $this->vars['urlCode'] = $urlCode;

        list($author, $plugin) = explode('-', $urlCode);

        $manager = PluginManager::instance();
        $pluginDetails = $manager
            ->findByIdentifier($author . '.' . $plugin)
            ->pluginDetails();

        $this->vars['primary_lang'] = isset($pluginDetails['lang']) ? $pluginDetails['lang'] : '';

        $langDir = implode(DIRECTORY_SEPARATOR, [base_path(), 'plugins', $author, $plugin, 'lang']);
        $languages = glob($langDir . '/*', GLOB_ONLYDIR);

        array_walk($languages, array($this, 'trimDir'), strlen($langDir) + 1);

        $this->vars['langs'] = array_combine($languages, $languages);
    }

    public function onCompare($urlCode = null)
    {
        $this->vars['urlCode'] = $urlCode;

        list($author, $plugin) = explode('-', $urlCode);

        if (post('lang1')) {

            $lang1 = post('lang1');
            $lang2 = post('lang2');

            $file1 = implode(DIRECTORY_SEPARATOR, [base_path(), 'plugins', $author, $plugin, 'lang', $lang1, 'lang.php']);

            $file2 = implode(DIRECTORY_SEPARATOR, [base_path(), 'plugins', $author, $plugin, 'lang', $lang2, 'lang.php']);

            $a1 = include $file1;

            $a2 = include $file2;

            $tk1 = $this->getTransKeys($a1);
            $tk2 = $this->getTransKeys($a2);

            $this->vars['lang1'] = $lang1;
            $this->vars['dif1'] = array_diff_key($tk1, $tk2);

            $this->vars['lang2'] = $lang2;
            $this->vars['dif2'] = array_diff_key($tk2, $tk1);

        }

    }

    public function onView($urlCode = null)
    {
        $this->vars['urlCode'] = $urlCode;

        list($author, $plugin) = explode('-', $urlCode);

        if (post('lang1')) {

            $lang1 = post('lang1');

            $file1 = implode(DIRECTORY_SEPARATOR, [base_path(), 'plugins', $author, $plugin, 'lang', $lang1, 'lang.php']);

            $a1 = include $file1;

            $tk1 = $this->getTransKeys($a1);
            $tk2 = $this->getTransKeys($a2);

            $this->vars['lang1'] = $lang1;
            $this->vars['dif1'] = array_diff($tk1, $tk2);

            $this->vars['lang2'] = $lang2;
            $this->vars['dif2'] = array_diff($tk2, $tk1);

        }

    }
}

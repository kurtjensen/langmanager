<?php
return [
    'plugin' => [//Plugin File
        'short_name' => 'Language',
        'name' => 'Language Manager',
        'description' => 'Manage the language files for your plugins.',
        'Main_btn' => 'Lang Manager',

        'perm_tab' => 'Language Manager',
        'perm_all' => 'All',
        'primary' => 1,
    ],
    'result' => [
        'explain' => 'The items listed below are in the ":lang2" file but not in the ":lang1" file',
    ],
    'update' => [
        'compare' => 'Compare',
        'plugin' => 'Plugin',
        'lang_avail' => 'Languages Available',
        'auth_primary' => 'Author has indicated that this plugins primary Language is : ',
        'primary_lang' => 'Primary Language',
        'second_lang' => 'Language to Compare',
    ],
];
